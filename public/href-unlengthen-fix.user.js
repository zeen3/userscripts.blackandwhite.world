// ==UserScript==
// @name     link unlengthen and fix
// @match    https://mail.google.com/*
// @match    https://*.reddit.com/*
// @match    https://reddit.com/*
// @version  1.0.0
// @grant    none
// @run-at   document-start
// ==/UserScript==

const noutm = el => {
  let np = new URLSearchParams([]);
  for (let [key, val] of new URLSearchParams(el.search)) {
    if (!key.startsWith('utm_')) {
      np.append(key, val);
    }
  }
  let sp = np.toString();
  el.search = sp && ('?' + sp);
}
{
  const eldo = el => {
    if (el instanceof HTMLAnchorElement) {
      if (el.host === 'www.google.com' && el.pathname === '/url') {
        el.href = new URLSearchParams(el.search).get('q');
      }
      if (el.host === 'click.redditmail.com') {
        el.href = decodeURIComponent(el.pathname.slice('/CL0/'.length));
      }
      if (el.host.endsWith('.reddit.com') || el.host === "reddit.com") {
        el.host = 'old.reddit.com';
      }
      if (el.host === 'www.youtube.com' && el.pathname === '/attribution_link') {
        let url = new URL(el.href);
        url.href = el.origin + url.searchParams.get('u');
        url.searchParams.delete('feature');
        el.href = url.href;
      }
      if (el.host === 'dev.to' && el.pathname.startsWith('/ahoy') && el.pathname.endsWith('/click')) {
        let url = new URL(new URLSearchParams(el.search).get('url'), 'https://dev.to/');
        el.href = url.href;
        noutm(el);
      }
    }
    s.add(el);
  };
  const _raf = _ => requestAnimationFrame(_);
  const raf = () => new Promise(_raf);
  let iters = 64;
  const s = new WeakSet();
  const next = () => {
    for (let link of document.links) if (!s.has(link)) eldo(link);
    iters-- && requestAnimationFrame(next)
  };
  requestAnimationFrame(next);
  document.addEventListener('load', () => void(iters = 8, next()), {once: true});
}
