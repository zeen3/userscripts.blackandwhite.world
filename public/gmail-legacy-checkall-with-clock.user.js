// ==UserScript==
// @name         GMail Legacy
// @namespace    https://ripsters.zeen3.xyz/
// @version      0.6
// @description  adds select all buttons, a clock, and makes the titles selectable.
// @author       zeen3
// @match        https://mail.google.com/*
// @run-at       document-start
// @grant        none
// @require      https://ripsters.zeen3.xyz/ripster-shared.js
// ==/UserScript==

'use strict';
const {nt, $$, $, $ce, $cec} = self[self['next-tick']].suicide();
let i = 63;

requestAnimationFrame(function __checkall() {
  try {
    const t = $('table.th', document.forms.f, HTMLTableElement);
    const _time = [new Date(), 0>>>0, -1, ""];
    const ids = [];
    const marked = new WeakSet;
    let is_changing = false;
    function on_change_t() {
      if (!is_changing) {
        is_changing = true;
        if (this.checked) {
          let check = Array.prototype.every.call(document.getElementsByName('t'), i => i.checked);
          for (const e of ids) e.checked = check;
        } else {
          for (const e of ids) e.checked = false;
        }
        is_changing = false;
      }
    }
    function on_change_all() {
      if (!is_changing) {
        is_changing = true;
        const {checked} = this;
        for (const e of ids) if (e !== this) e.checked = checked;
        for (const e of document.getElementsByName('t')) e.checked = checked;
        is_changing = false;
      }
    }
    const create = tsec => {
      const id = '__' + Math.random().toString(36).slice(2);
      const time = document.createTextNode(_time[3]);
      let __time = _time[2] >>> 0;
      requestAnimationFrame(function set_time() {
        if (__time !== _time[2]) time.data = _time[3];
        __time = _time[2] >>> 0;
        requestAnimationFrame(set_time);
      });
      tsec.style = "position:sticky;top:0;left:0;bottom:0;background:inherit;";
      let toggle = $ce('input', {
        "id": id,
        "type": 'checkbox',
        "onchange": on_change_all,
      });
      ids.push(toggle);
      tsec.append(
        $cec('td', [
          toggle,
          $ce('img', {
            "src": 'https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif',
            "width": 15,
            "height": 15,
            "border": 0,
            "alt": ''
          })
        ]),
        $cec('td', [$cec('label', ['Check all'], {"htmlFor": id, "style": "display:block;cursor:pointer;"})]),
        $cec('td', [time], {"colSpan": 2, "style": "font:13px monospace;text-align:right;"})
      );
    };
    create(t.createTFoot());
    create(t.createTHead());
    document.head.append($ce("style", {
      "innerHTML":
        `.th tbody tr:last-child td { border-bottom: 0; }` +
        `.th tfoot td { border-top: 1px solid #bbb; }`
    }));

    requestAnimationFrame(function set_time() {
      _time[0].setTime(Date.now());
      _time[1] = _time[0].getSeconds() >>> 0;
      if (_time[1] !== _time[2]) _time[3] = _time[0].toUTCString();
      _time[2] = _time[1] >>> 0;
      requestAnimationFrame(set_time);
    });
    i = 127;
    requestAnimationFrame(function __call_onchange_t() {
      for (const e of document.getElementsByName('t')) {
        if (marked.has(e)) continue;
        marked.add(e);
        e.addEventListener('change', on_change_t);
      }
      i-- && requestAnimationFrame(__call_onchange_t);
    });
    requestAnimationFrame(() => {
      let left = $$('table.th tbody tr[bgcolor] td:first-child input', document.forms.f, HTMLInputElement);
      let right = $$('table.th tbody tr[bgcolor] td:nth-child(2)', document.forms.f, HTMLTableCellElement);
      let lo = Math.min(left.length, right.length) | 0;
      for (let i = 0; i < lo; ++i) {
        let ip = left[i], ts = right[i];
        if (ts.firstChild === ts.lastChild) {
          ip.id = `__${Math.random().toString(36).slice(2)}`;
          ts.append($cec('label', [ts.firstChild], {"htmlFor": ip.id, "style": "display:block;cursor:pointer;"}));
        }
      }
    });
  } catch(e) {
    console.debug(e);
    if (i--) requestAnimationFrame(__checkall);
    else console.debug(i);
  }
});
